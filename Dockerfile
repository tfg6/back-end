FROM python:3.8 as build

ENV DEBUG=False \
    SECRET_KEY= \
    APP_URL=http://localhost \
    DB_ENGINE= \
    DB_NAME= \
    DB_USER= \
    DB_PASSWORD= \
    DB_HOST= \
    DB_PORT=

# set work directory
WORKDIR /usr/src/app

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt .
RUN pip install -r requirements.txt
RUN apt-get update && apt-get install -y cron && touch /var/log/cron.log
RUN sed -i 's/\/bin\/sh/\/bin\/bash/' /etc/crontab

FROM build

# copy de project into container
COPY . .

EXPOSE 5005
CMD ["sh", "-c", "cron && env > .env && python manage.py crontab add && gunicorn --config gunicorn-cfg.py core.wsgi"]
