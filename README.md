# Neural Runner

This poryects allow to run dockerized neural networks.

## Table of Contents

- [Neural Runner](#neural-runner)
  - [Table of Contents](#table-of-contents)
  - [Quick start](#quick-start)
  - [Documentation](#documentation)
    - [Databases configuration](#databases-configuration)
      - [SQLITE](#sqlite)
      - [MySQL/MariaDB](#mysqlmariadb)
      - [PostgreSQL](#postgresql)
      - [Oracle](#oracle)
  - [File Structure](#file-structure)
  - [Browser Support](#browser-support)
  - [Resources](#resources)
  - [Reporting Issues](#reporting-issues)

<br />

<br />

## Quick start

> **PREREQUISITES** You need `docker` instaled in your system to deploy this web tool

Create a volume, so the web tool could interact with child containers.

```bash
# Volume to interact bettwen containers and the web tool
$ docker volume create --name=media
```

Create a nginx.conf and paste the following code.

```conf
server {
    listen 80;

    location / {
        proxy_pass http://back-end:5005/;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

}
```

Create a docker-compose.yaml and paste the following code.

```yaml
version: "3"
services:
    back-end:
        image: registry.gitlab.com/tfg6/back-end:latest
        environment:
            SECRET_KEY: Sup3rS3cr3tK3y
            APP_URL: localhost
        volumes:
            - media:/usr/src/app/media
            - /var/run/docker.sock:/var/run/docker.sock
        expose:
            - 5005
        networks:
            - web_network
        depends_on:
            - db

    nginx:
        image: "nginx:latest"
        ports:
            - "80:80"
        volumes:
            - ./nginx.conf:/etc/nginx/conf.d
        networks:
            - web_network
        depends_on:
            - back-end

networks:
    web_network:
        driver: bridge

volumes:
    media:
        external: true
```

Then you can deploy de aplication with the following commands

```bash
# Start the web tool
$ docker-compose up -d
```

```bash
# Create tables in the database
$ docker-compose exec back-end python manage.py migrate
```

```bash
# Create super user to access the admin pages
$ docker-compose exec back-end python manage.py createsuperuser
```

Access the web app in a browser: [http://127.0.0.1/](http://127.0.0.1/)

<br />

## Documentation

-   [Databases](#database-configuration)

### Databases configuration

The next configurations enable a conection to one specific database.

#### SQLITE

```env
DB_ENGINE: django.db.backends.sqlite3
```

#### MySQL/MariaDB

```env
DB_ENGINE: django.db.backends.postgresql
DB_NAME: <database_name>
DB_USER: <user>
DB_PASSWORD: <password>
DB_HOST: <host>
DB_PORT: 3306
```

#### PostgreSQL

```env
DB_ENGINE: django.db.backends.postgresql
DB_NAME: <database_name>
DB_USER: <user>
DB_PASSWORD: <password>
DB_HOST: <host>
DB_PORT: 5432
```

#### Oracle

```env
DB_ENGINE: django.db.backends.oracle
DB_NAME: <database_name>
DB_USER: <user>
DB_PASSWORD: <password>
DB_HOST: <host>
DB_PORT: 1521
```

<br />

## File Structure

Within the download you'll find the following directories and files:

```bash
< PROJECT ROOT >
   |
   |-- core/                               # Implements app logic and serve the static assets
   |    |-- settings.py                    # Django app bootstrapper
   |    |-- wsgi.py                        # Start the app in production
   |    |-- urls.py                        # Define URLs served by all apps/nodes
   |    |
   |    |-- schemas/                       # Schemas to configure some forms
   |    |    |-- <json>
   |    |
   |    |-- static/
   |    |    |-- <css, JS, images>         # CSS files, Javascripts files
   |    |
   |    |-- templates/                     # Templates used to render pages
   |         |
   |         |-- includes/                 # HTML chunks and components
   |         |    |-- footer.html          # Footer component
   |         |    |-- navigation.html      # Top menu component
   |         |    |-- scripts-sidebar.html # Scripts for sidebar
   |         |    |-- scripts.html         # Scripts common to all pages
   |         |    |-- sidebar.html         # Sidebar component
   |         |
   |         |-- layouts/                  # Master pages
   |         |    |-- base-login.html # Used by Authentication pages
   |         |    |-- base.html            # Used by common pages
   |         |
   |         |-- accounts/                 # Authentication pages
   |         |    |-- login.html           # Login page
   |         |    |-- register.html        # Register page
   |         |
   |         |-- page-403.html             # Error 403 page
   |         |-- page-404.html             # Error 404 page
   |         |-- page-500.html             # Error 500 page
   |         |-- *.html                    # All other HTML pages
   |
   |-- authentication/                     # Handles auth routes (login and register)
   |    |
   |    |-- admin.py                       # Admin display for users
   |    |-- apps.py                        # Authentification configuration
   |    |-- forms.py                       # Define auth forms
   |    |-- models.py                      # Define auth models
   |    |-- test.py                        # Define authentication unit test
   |    |-- urls.py                        # Define authentication routes
   |    |-- views.py                       # Handles login and registration
   |
   |-- app/                                # A simple app that serve HTML files
   |    |-- admin.py                       # Admin display for app components
   |    |-- apps.py                        # App configuration
   |    |-- cron.py                        # Funtions toperform automations
   |    |-- fields.py                      # Custon fields
   |    |-- forms.py                       # App forms
   |    |-- models.py                      # Models definitions
   |    |-- tests.py                       # Unit tests for app
   |    |-- urls.py                        # Urls for app
   |    |-- views.py                       # Views for app
   |    |-- widtgets.py                    # Custom widgets forms
   |    |-- migrations                     # Migrations defined form models
   |         |-- 0001_initial.py
   |         |-- 0002_model_schema.py
   |         |-- 0003_auto_20210212_2258.py
   |         |-- 0004_auto_20210303_1842.py
   |
   |-- requirements.txt                    # Development modules - SQLite storage
   |
   |-- .env                                # Inject Configuration via Environment
   |-- manage.py                           # Start the app - Django default start script
   |
   |-- ************************************************************************
```

<br />

## Browser Support

At present, we officially aim to support the last two versions of the following browsers:

<img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/chrome.png" width="64" height="64"> <img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/firefox.png" width="64" height="64"> <img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/edge.png" width="64" height="64"> <img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/safari.png" width="64" height="64"> <img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/opera.png" width="64" height="64">

<br />

## Resources

-   [Docker](https://docs.docker.com/)
-   [Docker Compose](https://docs.docker.com/compose/)
-   [Django](https://docs.djangoproject.com/en/3.2/)
-   [Material Dashboard (Creative Tim)](https://demos.creative-tim.com/material-dashboard-django/docs/1.0/getting-started/getting-started-django.html)

<br />

## Reporting Issues

We use Gitlab Issues as the official bug tracker for this tool. Here are some advices for our users that want to report an issue:

1. Providing us reproducible steps for the issue will shorten the time it takes for it to be fixed.
2. Some issues may be browser-specific, so specifying in what browser you encountered the issue might help.

<br />
