var defects_list
var selected_dragable

const inputX = document.getElementById("x")
const inputY = document.getElementById("y")
const inputW = document.getElementById("width")
const inputH = document.getElementById("height")
const inputP = document.getElementById("permeability")
const inputParams = document.getElementById("id_parameters")
const inputDefects = document.getElementById("defects")

const square = document.getElementById("square")
const draggable = document.getElementsByClassName("draggable")

inputDefects.addEventListener("change", function () {
    console.log(inputDefects.value)
    if (inputDefects.value > 0) {
        defects_list = []
        for (let i = 0; i < inputDefects.value; i++) {
            defects_list.push([
                50,
                50,
                Math.floor(Math.random() * 25 + 10),
                Math.floor(Math.random() * 25 + 10),
                Math.random().toFixed(2),
            ])
        }
    } else {
        defects_list = [[0.0, 0.0, 0.0, 0.0, 0.0]]
    }
    inputParams.value = JSON.stringify(defects_list)
    createDefect(defects_list)
})

inputX.addEventListener("change", function () {
    var newX =
            parseInt(inputToCanvas(inputX.value)) -
            parseInt(inputToCanvas(inputW.value) / 2),
        newY =
            parseInt(inputToCanvas(inputY.value)) -
            parseInt(inputToCanvas(inputH.value) / 2)
    selected_dragable.setAttribute("data-x", newX)
    selected_dragable.setAttribute("data-y", newY)
    selected_dragable.style.webkitTransform = selected_dragable.style.transform =
        "translate(" + newX + "px," + newY + "px)"
    updateElement(selected_dragable)
})

inputY.addEventListener("change", function () {
    var newX =
            parseInt(inputToCanvas(inputX.value)) -
            parseInt(inputToCanvas(inputW.value) / 2),
        newY =
            parseInt(inputToCanvas(inputY.value)) -
            parseInt(inputToCanvas(inputH.value) / 2)
    selected_dragable.setAttribute("data-x", newX)
    selected_dragable.setAttribute("data-y", newY)
    selected_dragable.style.webkitTransform = selected_dragable.style.transform =
        "translate(" + newX + "px," + newY + "px)"
    updateElement(selected_dragable)
})

inputW.addEventListener("change", function () {
    selected_dragable.style.width = inputToCanvas(inputW.value) + "px"
    updateElement(selected_dragable)
})

inputH.addEventListener("change", function () {
    selected_dragable.style.height = inputToCanvas(inputH.value) + "px"
    updateElement(selected_dragable)
})

inputP.addEventListener("change", function (event) {
    selected_dragable.setAttribute("data-p", inputP.value)
    selected_dragable.style.backgroundColor =
        "rgb(" +
        parseInt(255 * inputP.value) +
        "," +
        parseInt(255 * inputP.value) +
        "," +
        parseInt(255 * inputP.value) +
        ")"
    updateElement(selected_dragable)
})

function dragMoveListener(event) {
    var target = event.target
    // keep the dragged position in the data-x/data-y attributes
    var x = (parseFloat(target.getAttribute("data-x")) || 0) + event.dx
    var y = (parseFloat(target.getAttribute("data-y")) || 0) + event.dy

    // translate the element
    target.style.webkitTransform = target.style.transform =
        "translate(" + x + "px, " + y + "px)"

    // update the posiion attributes
    target.setAttribute("data-x", x)
    target.setAttribute("data-y", y)

    getDragableValues(event.target)
}

function resizeListener(event) {
    var target = event.target
    var x = parseFloat(target.getAttribute("data-x")) || 0
    var y = parseFloat(target.getAttribute("data-y")) || 0

    // update the element's style
    target.style.width = event.rect.width + "px"
    target.style.height = event.rect.height + "px"

    // translate when resizing from top or left edges
    x += event.deltaRect.left
    y += event.deltaRect.top

    target.style.webkitTransform = target.style.transform =
        "translate(" + x + "px," + y + "px)"

    target.setAttribute("data-x", x)
    target.setAttribute("data-y", y)

    getDragableValues(event.target)
}

function getDragableValues(target) {
    $(".draggable").removeClass("active")
    $(target).addClass("active")

    selected_dragable = target

    var x = parseFloat(target.getAttribute("data-x")) || 0
    var y = parseFloat(target.getAttribute("data-y")) || 0

    inputW.value = canvasToInput(target.offsetWidth)
    inputH.value = canvasToInput(target.offsetHeight)
    inputX.value = parseInt(canvasToInput(x)) + parseInt(inputW.value / 2)
    inputY.value = parseInt(canvasToInput(y)) + parseInt(inputH.value / 2)
    inputP.value = target.getAttribute("data-p")

    updateElement(target)
}

function updateElement(target) {
    var n = target.id.split("_")[1]
    defects_list[n] = [
        inputX.value,
        inputY.value,
        inputW.value,
        inputH.value,
        inputP.value,
    ]

    inputParams.value = JSON.stringify(defects_list)
}

function inputToCanvas(value) {
    var width = document.getElementById("square").getBoundingClientRect().width
    return parseInt((value * width) / 100)
}

function canvasToInput(value) {
    var width = document.getElementById("square").getBoundingClientRect().width
    return parseInt((value * 100) / width)
}

function createDefect(defects) {
    while (square.lastElementChild) {
        square.removeChild(square.lastElementChild)
    }

    for (let n = 0; n < defects.length; n++) {
        var newDefect = document.createElement("div")
        newDefect.id = "defect_" + n
        newDefect.classList.add("draggable")

        newDefect.setAttribute(
            "data-x",
            inputToCanvas(defects[n][0]) - inputToCanvas(defects[n][2]) / 2
        )
        newDefect.setAttribute(
            "data-y",
            inputToCanvas(defects[n][1]) - inputToCanvas(defects[n][3]) / 2
        )

        newDefect.setAttribute("data-p", defects[n][4])

        newDefect.style.width = inputToCanvas(defects[n][2]) + "px"
        newDefect.style.height = inputToCanvas(defects[n][3]) + "px"
        newDefect.style.transform =
            "translate(" +
            parseInt(
                inputToCanvas(defects[n][0]) - inputToCanvas(defects[n][2]) / 2
            ) +
            "px," +
            parseInt(
                inputToCanvas(defects[n][1]) - inputToCanvas(defects[n][3]) / 2
            ) +
            "px)"
        newDefect.style.backgroundColor =
            "rgb(" +
            parseInt(255 * defects[n][4]) +
            "," +
            parseInt(255 * defects[n][4]) +
            "," +
            parseInt(255 * defects[n][4]) +
            ")"
        square.appendChild(newDefect)
    }
    start()
}

function start() {
    interact(".draggable")
        .resizable({
            // resize from all edges and corners
            edges: { left: true, right: true, bottom: true, top: true },

            listeners: {
                move: resizeListener,
            },
            modifiers: [
                // keep the edges inside the parent
                interact.modifiers.restrictEdges({
                    outer: "parent",
                }),

                // minimum size
                interact.modifiers.restrictSize({
                    min: { width: 5, height: 5 },
                }),
            ],
        })
        .draggable({
            listeners: { move: dragMoveListener },
            modifiers: [
                interact.modifiers.restrictRect({
                    restriction: "parent",
                    endOnly: true,
                }),
            ],
        })
        .on("tap", function (event) {
            getDragableValues(event.target)
            event.preventDefault()
        })
}
