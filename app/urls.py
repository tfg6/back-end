# -*- encoding: utf-8 -*-


from django.urls import path, re_path
from .views import *
from django.views.generic.base import RedirectView

urlpatterns = [

    # The home page
    path('', SimulationsView.as_view(), name='home'),

    path('simulations', SimulationsView.as_view(), name='simulations'),
    path('simulations/new', NewSimulationView.as_view(), name='new-simulation'),
    path('simulations/<int:simulation_id>/',
         SimulationView.as_view(), name='simulation'),
    path('simulations/<int:simulation_id>/delete',
         delete_simulation, name='delete-simulation'),
    path('simulations/<int:simulation_id>/outputs/<int:output_id>/',
         OutputView.as_view(), name='output'),
    path('simulations/<int:simulation_id>/outputs/<int:output_id>/files/<path:output_file>',
         OutputFileView.as_view(), name='output-file'),
    path('simulations/<int:simulation_id>/outputs/<int:output_id>/log',
         OutputLogView.as_view(), name='output-log'),
    path('models', ModelsView.as_view(), name='models'),
    path('models/<int:model_id>/', ModelView.as_view(), name='model'),

]
