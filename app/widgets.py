from django.forms import widgets
from django import forms


class MaterialFilesInput(widgets.FileInput):
    template_name = 'forms/widgets/material-files-input.html'


# class DraggableBoxWidget(widgets.MultiWidget):
#     template_name = 'forms/widgets/draggable-box-input.html'

#     def __init__(self, *args, **kwargs):
#         widgets = {
#             'x': forms.NumberInput(attrs={'min': 0, 'max': 100, 'step': 1, 'placeholder': 0, 'name': 'X'}),
#             'y': forms.NumberInput(attrs={'min': 0, 'max': 100, 'step': 1, 'placeholder': 0, 'name': 'Y'}),
#             'width': forms.NumberInput(attrs={'min': 0, 'max': 100, 'step': 1, 'placeholder': 0, 'name': 'Width'}),
#             'height': forms.NumberInput(attrs={'min': 0, 'max': 100, 'step': 1, 'placeholder': 0, 'name': 'Height'}),
#             'permeability': forms.NumberInput(attrs={'min': 0, 'max': 1, 'step': 0.01, 'placeholder': 0, 'name': 'Permeability'})
#         }
#         super(DraggableBoxWidget, self).__init__(widgets, *args, **kwargs)

#     def decompress(self, value):
#         if value:
#             return value.split(',')
#         return [None, None, None, None, None]

#     class Media:
#         js = (  # 'https://cdn.jsdelivr.net/npm/interactjs/dist/interact.min.js',
#             'https://cdn.jsdelivr.net/npm/interactjs@1.10.5/dist/interact.min.js',
#             'assets/js/draggable-box-input.js')


class DraggableBoxWidget(widgets.HiddenInput):
    template_name = 'forms/widgets/draggable-box-input.html'

    class Media:
        js = (  # 'https://cdn.jsdelivr.net/npm/interactjs/dist/interact.min.js',
            'https://cdn.jsdelivr.net/npm/interactjs@1.10.5/dist/interact.min.js',
            'assets/js/draggable-box-input.js')
