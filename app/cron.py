from django.core.files.storage import default_storage
from django.core.files.base import ContentFile, File

import logging
import docker

from .models import *


def check_simulations():

    print('[CHECK-SIMULATIONS]: Start')

    simulations = Simulation.objects.exclude(
        state__in=[STATE_FALIED, STATE_PASSED])

    print(simulations)

    client = docker.from_env()

    for simulation in simulations.iterator():

        container = client.containers.get(simulation.container)

        # logging.info(f'[CHECK-SIMULATIONS]: check simulation <{container.id}>')
        # posible status [created, restarting, running, removing, paused, exited, dead]
        if container.status == 'exited':
            # logging.info(f'[CHECK-SIMULATIONS]: simulation <{container.id}> finished')

            result = container.wait()

            # Save logs form container
            logs = container.logs()

            if result['StatusCode'] == 0:
                simulation.state = STATE_PASSED

                # Save result form container
                stream, stats = container.get_archive('/output')
                with open(f'/tmp/{container.id}.tgz', 'wb') as result:
                    for chunk in stream:
                        result.write(chunk)

                # Create output with logs and results
                out = simulation.outputs.create(
                    result=default_storage.save(
                        f'results/{container.id}.tgz', ContentFile(open(f'/tmp/{container.id}.tgz', 'rb').read())),
                    log=default_storage.save(
                        f'logs/{container.id}.txt', ContentFile(logs))
                )
            else:
                simulation.state = STATE_FALIED

                # Create output only with logs to debug errors
                simulation.outputs.create(
                    result=None,
                    log=default_storage.save(
                        f'logs/{container.id}.txt', ContentFile(logs))
                )
            simulation.save()

        elif container.status in ['created', 'running']:
            simulation.state = STATE_RUNNING
            simulation.save()

    print('[CHECK-SIMULATIONS]: End')
