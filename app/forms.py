from django import forms

from django_jsonforms.forms import JSONSchemaField

from .models import *
from .fields import *
from .widgets import *


class SimulationForm(forms.ModelForm):
    model = forms.ModelChoiceField(
        queryset=Model.objects.all(),
        widget=forms.Select(
            attrs={
                'class': 'form-control'
            }
        ),
        required=True,
        label='Model'
    )

    # parameters = DraggableBoxField()
    parameters = forms.JSONField(
        initial=[[0.0, 0.0, 0.0, 0.0, 0.0]], widget=DraggableBoxWidget())

    # files = forms.FileField(
    #     widget=MaterialFilesInput()
    # )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # self.fields['model'] = forms.ModelChoiceField(
        #     queryset=Model.objects.all(),
        #     widget=forms.Select(
        #         attrs={
        #             'class': 'form-control'
        #         }
        #     ),
        #     required=True,
        #     label='Model'
        # )

        # for i in range(5):
        #     self.fields[f'files_{i}'] = forms.FileField(
        #         widget=MaterialFilesInput(attrs={
        #             'class': 'form-file-upload form-file-multiple'
        #         })
        #     )

        # self.fields['box'] = DraggableBoxField()

    def get_dynamic_fields(self):
        for field_name in self.fields:
            yield self[field_name]

    class Meta:
        model = Simulation
        fields = ['model']


class ModelForm(forms.ModelForm):
    schema = JSONSchemaField(
        schema='model.json',
        options='options.json',
        ajax=False
    )

    class Meta:
        model = Model
        fields = '__all__'
