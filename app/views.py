# -*- encoding: utf-8 -*-


from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader
from django.http import HttpResponse
from django import template
from django.views import generic
from django.core.files.base import ContentFile, File
from django.core.files.storage import default_storage
from django.conf import settings
from django.http import HttpResponse, Http404


import docker
from io import StringIO
import tarfile
import base64
from mimetypes import MimeTypes
import pandas as pd
import uuid
import os
import json

from .models import *
from .forms import *


class SimulationsView(LoginRequiredMixin, generic.ListView):
    model = Simulation
    template_name = 'simulations.html'

    context_object_name = 'simulations_list'

    def get_queryset(self):
        return self.request.user.simulations.all().order_by('-created_at')


class SimulationView(LoginRequiredMixin, generic.DetailView):
    pk_url_kwarg = 'simulation_id'
    model = Simulation
    template_name = 'simulation.html'


def delete_simulation(request, simulation_id):
    simulation = request.user.simulations.get(pk=simulation_id)
    client = docker.from_env()
    try:
        container = client.containers.get(simulation.container).remove()
    except:
        print('Remove container Error')

    if simulation.parameters and os.path.exists(simulation.parameters.path):
        os.remove(simulation.parameters.path)
        os.rmdir(os.path.dirname(simulation.parameters.path))

    for output in simulation.outputs.all().iterator():

        if output.result and os.path.exists(output.result.path):
            os.remove(output.result.path)

        if output.log and os.path.exists(output.log.path):
            os.remove(output.log.path)

    simulation.delete()
    return redirect('simulations')


class NewSimulationView(LoginRequiredMixin, generic.CreateView):
    model = Simulation
    template_name = 'new-simulation.html'

    form_class = SimulationForm

    def form_valid(self, form):
        form.instance.user = self.request.user

        client = docker.from_env()

        model = form.cleaned_data['model']

        print(model.schema)
        print(model.schema['input_dir'])
        print(model.schema['gpu'])

        # Convert input in csv table
        data = form.cleaned_data['parameters']

        df = pd.DataFrame(data=data)

        inputs_id = uuid.uuid4()
        input_path = f'inputs/{inputs_id}/'
        form.instance.parameters = default_storage.save(
            input_path + 'input.csv', ContentFile(df.to_csv(index=False)))

        input_dir = f"{model.schema['input_dir']}/inputs/{inputs_id}"

        env_var = [
            *model.schema['env_var'],
            {'name': 'INPUT_DIR', 'value': input_dir},
            {'name': 'OUTPUT_DIR', 'value': model.schema['output_dir']}
        ]
        print("ENVIRONMENT", [
              f"{env['name']}={env['value']}" for env in env_var])

        container = client.containers.run(
            form.instance.model.image,
            detach=True,
            stderr=True,
            # Enviroment variables
            environment=[f"{env['name']}={env['value']}" for env in env_var],
            # Mount volumes
            volumes={'media': {
                'bind': model.schema['input_dir'], 'mode': 'ro'}},
            # Model use gpu to inprove performace
            runtime='nvidia' if model.schema['gpu'] else None,
        )

        form.instance.container = container.id

        super(NewSimulationView, self).form_valid(form)

        return redirect('simulations')


class OutputView(LoginRequiredMixin, generic.DetailView):
    pk_url_kwarg = 'output_id'
    model = Output
    template_name = 'output.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        result = tarfile.open(self.object.result.path, 'r')
        context['files'] = result.getmembers()
        return context


class OutputFileView(LoginRequiredMixin, generic.DetailView):
    pk_url_kwarg = 'output_id'
    model = Output
    template_name = 'output-file.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        result = tarfile.open(self.object.result.path, 'r')
        context['name'] = self.kwargs['output_file']
        context['mime'], _ = MimeTypes().guess_type(self.kwargs['output_file'])
        context['type'] = context['mime'].split('/')[0]
        context['file'] = result.extractfile(self.kwargs['output_file']).read()
        return context


def dowload_file():
    pass


class OutputLogView(LoginRequiredMixin, generic.DetailView):
    pk_url_kwarg = 'output_id'
    model = Output
    template_name = 'output-log.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['log_content'] = open(self.object.log.path, 'r').read()

        return context


class ModelsView(LoginRequiredMixin, generic.ListView):
    model = Model
    template_name = 'models.html'
    context_object_name = 'models_list'


class ModelView(LoginRequiredMixin, generic.DetailView):
    pk_url_kwarg = 'model_id'
    model = Model
    template_name = 'model.html'
