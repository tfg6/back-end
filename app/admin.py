# -*- encoding: utf-8 -*-


from django.contrib import admin

from .models import *
from .forms import *

# Register your models here.


@admin.register(Model)
class ModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'image', 'created_at', 'updated_at')
    form = ModelForm
    class Media:
        js = [
            '//code.jquery.com/jquery-3.5.1.min.js',
            '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'
        ]
        css = {
            'all': (
                '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
                '//pro.fontawesome.com/releases/v5.15.2/css/all.css'
            )
        }


@admin.register(Simulation)
class SimulationAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'model', 'container', 'parameters',
                    'state', 'created_at')


@admin.register(Output)
class OutputAdmin(admin.ModelAdmin):
    list_display = ('id', 'simulation', 'result', 'log', 'created_at')
