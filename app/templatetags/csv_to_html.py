from django import template
from django.template.defaultfilters import safe
import pandas as pd
import io

register = template.Library()


@register.filter
def csv_to_html(value):
    pd.set_option('colheader_justify', 'left')
    return safe(pd.read_csv(io.BytesIO(value)).to_html(classes='table', border=0))
