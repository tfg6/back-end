from django import template
from django.template.defaultfilters import safe

import base64
register = template.Library()


@register.filter
def b64encode(value):
    return base64.b64encode(value).decode('ascii')
