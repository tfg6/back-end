from django import template
from django.template.defaultfilters import safe
import markdown as mk

register = template.Library()


@register.filter
def markdown(value):
    return safe(mk.markdown(value, extensions=['extra']))
