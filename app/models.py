# -*- encoding: utf-8 -*-


from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils.translation import gettext as _


class Model(models.Model):
    name = models.CharField('Name', max_length=200)
    description = models.TextField('Description', null=True, blank=True)
    image = models.CharField('Image', max_length=200)
    schema = models.JSONField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('model')
        verbose_name_plural = _('models')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('model', kwargs={'model_id': self.pk})


STATE_PENDING = 0
STATE_RUNNING = 1
STATE_PASSED = 2
STATE_FALIED = 3
STATE_CHOICES = (
    (STATE_PENDING, 'Pending'),
    (STATE_RUNNING, 'Running'),
    (STATE_PASSED, 'Passed'),
    (STATE_FALIED, 'Falied')
)


class Simulation(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='simulations')
    model = models.ForeignKey(
        Model, on_delete=models.CASCADE, related_name='simulations')
    container = models.CharField('Container', max_length=256)
    parameters = models.FileField(upload_to='inputs/')
    state = models.IntegerField(choices=STATE_CHOICES, default=STATE_PENDING)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Simulation')
        verbose_name_plural = _('Simulations')

    def __str__(self):
        return u'{0}'.format(self.id)

    def get_absolute_url(self):
        return reverse('simulation', kwargs={'simulation_id': self.pk})


class Output(models.Model):
    simulation = models.ForeignKey(
        Simulation, on_delete=models.CASCADE, related_name='outputs')
    result = models.FileField(upload_to='results/')
    log = models.FileField(upload_to='logs/')
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Output')
        verbose_name_plural = _('Outputs')

    def __str__(self):
        return u'{0}'.format(self.id)

    def get_absolute_url(self):
        return reverse('output', kwargs={'output_id': self.pk})
